
<?php
// import highchart libray
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('http://code.highcharts.com/highcharts.js');
//$cs->registerScriptFile('http://code.highcharts.com/highcharts-more.js');

 $categories = array('0-4', '5-9', '10-14', '15-19',
            '20-24', '25-29', '30-34', '35-39', '40-44',
            '45-49', '50-54', '55-59', '60-64', '65-69',
            '70-74', '75-79', '80-84', '85-89', '90-94',
            '95-99', '100 + ');// กลุ่มอายุ
 
 $categories = implode("','", $categories);

$data_male = array(-1746181, -1884428, -2089758, -2222362, -2537431, -2507081, -2443179,
    -2664537, -3556505, -3680231, -3143062, -2721122, -2229181, -2227768,
    -2176300, -1329968, -836804, -354784, -90569, -28367, -3878);

$pop_male = implode(',', $data_male);

$data_female = array(1656154, 1787564, 1981671, 2108575, 2403438, 2366003, 2301402, 2519874,
    3360596, 3493473, 3050775, 2759560, 2304444, 2426504, 2568938, 1785638,
    1447162, 1005011, 330870, 130632, 21208);

$pop_female = implode(',', $data_female);

?>
<div id="div-chart" class="well">
    <!--    พื้นที่แสดง chart-->
</div>

<script>
    $(function () {
       

        $('#div-chart').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'ปิรามิดประชากรแบบ native javascript'
            },
            subtitle: {
                text: 'UTEHN PHNU'
            },
            xAxis: [{
                    categories: ['<?=$categories?>'],
                    reversed: false,
                    labels: {
                        step: 1
                    }
                }, {// mirror axis on right side
                    opposite: true,
                    reversed: false,
                    categories: ['<?=$categories?>'],
                    linkedTo: 0,
                    labels: {
                        step: 1
                    }
                }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return (Math.abs(this.value) / 1000000) + 'ล้าน';
                    }
                },
                min: -4000000,
                max: 4000000
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ', กลุ่มอายุ ' + this.point.category + '</b><br/>' +
                            'ประชากร: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
                }
            },
            series: [{
                    name: 'ชาย',
                    data: [<?= $pop_male ?>]
                }, {
                    name: 'หญิง',
                    data: [<?= $pop_female ?>]
                }]
        });


    });
</script>

