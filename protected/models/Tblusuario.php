<?php

/**
 * This is the model class for table "tblusuario".
 *
 * The followings are the available columns in table 'tblusuario':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $sexo
 * @property integer $celular
 * @property string $direccion
 * @property string $cargo
 * @property string $email
 * @property integer $iddep
 * @property integer $idmun
 * @property string $foto
 * @property string $estado
 * @property integer $cruge_user_iduser
 */
class Tblusuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tblusuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, nombre, apellido, sexo, celular, direccion, cargo, email, iddep, idmun, foto, estado, cruge_user_iduser', 'required'),
			array('id, celular, iddep, idmun, cruge_user_iduser', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>50),
			array('sexo', 'length', 'max'=>15),
			array('direccion', 'length', 'max'=>30),
			array('cargo', 'length', 'max'=>40),
			array('email', 'length', 'max'=>60),
			array('estado', 'length', 'max'=>10),
			/*
			//Example username
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                 'message'=>'Username can contain only alphanumeric 
                             characters and hyphens(-).'),
          	array('username','unique'),
          	*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, apellido, sexo, celular, direccion, cargo, email, iddep, idmun, foto, estado, cruge_user_iduser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'sexo' => 'Sexo',
			'celular' => 'Celular',
			'direccion' => 'Direccion',
			'cargo' => 'Cargo',
			'email' => 'Email',
			'iddep' => 'Iddep',
			'idmun' => 'Idmun',
			'foto' => 'Foto',
			'estado' => 'Estado',
			'cruge_user_iduser' => 'Cruge User Iduser',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('celular',$this->celular);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('cargo',$this->cargo,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('iddep',$this->iddep);
		$criteria->compare('idmun',$this->idmun);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('cruge_user_iduser',$this->cruge_user_iduser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tblusuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() 
    {
        $userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
		
		if($this->isNewRecord)
        {           
                        						
        }else{
                        						
        }

        
        return parent::beforeSave();
    }

    public function beforeDelete () {
		$userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
                                
        return false;
    }

    public function afterFind()    {
         
        parent::afterFind();
    }
	
		
	public function defaultScope()
    {
    	/*
    	//Example Scope
    	return array(
	        'condition'=>"deleted IS NULL ",
            'order'=>'create_time DESC',
            'limit'=>5,
        );
        */
        $scope=array();

        
        return $scope;
    }
}
