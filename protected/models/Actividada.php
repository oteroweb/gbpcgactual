<?php

/**
 * This is the model class for table "actividada".
 *
 * The followings are the available columns in table 'actividada':
 * @property integer $id
 * @property integer $idpro
 * @property string $codact
 * @property string $actividad
 * @property integer $valor1
 * @property integer $valor2
 * @property integer $varlor3
 * @property integer $valor4
 *
 * The followings are the available model relations:
 * @property Proyecto $idpro0
 * @property Indicador[] $indicadors
 */
class Actividada extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'actividada';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idpro, codact, actividad', 'required'),
			array('idpro, valor1, valor2, varlor3, valor4', 'numerical', 'integerOnly'=>true),
			array('codact', 'length', 'max'=>20),
			array('actividad', 'length', 'max'=>500),
			/*
			//Example username
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                 'message'=>'Username can contain only alphanumeric 
                             characters and hyphens(-).'),
          	array('username','unique'),
          	*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idpro, codact, actividad, valor1, valor2, varlor3, valor4', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idpro0' => array(self::BELONGS_TO, 'Proyecto', 'idpro'),
			'indicadors' => array(self::HAS_MANY, 'Indicador', 'idact'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{ 
		return array(
			'id' => 'ID',
			'idpro' => 'Idpro',
			'codact' => 'Codact',
			'actividad' => 'Actividad',
			'valor1' => 'Valor1',
			'valor2' => 'Valor2',
			'varlor3' => 'Varlor3',
			'valor4' => 'Valor4',
		);
	}
public static function ObtenerActividad() {
        return CHtml::listData(Proyecto::model()->findAll(), 'id', 'proyecto');
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idpro',$this->idpro);
		$criteria->compare('codact',$this->codact,true);
		$criteria->compare('actividad',$this->actividad,true);
		$criteria->compare('valor1',$this->valor1);
		$criteria->compare('valor2',$this->valor2);
		$criteria->compare('varlor3',$this->varlor3);
		$criteria->compare('valor4',$this->valor4);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Actividada the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() 
    {
        $userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
		
		if($this->isNewRecord)
        {           
                        						
        }else{
                        						
        }

        
        return parent::beforeSave();
    }

    public function beforeDelete () {
		$userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
                                
        return false;
    }

    public function afterFind()    {
         
        parent::afterFind();
    }
	
		
	public function defaultScope()
    {
    	/*
    	//Example Scope
    	return array(
	        'condition'=>"deleted IS NULL ",
            'order'=>'create_time DESC',
            'limit'=>5,
        );
        */
        $scope=array();

        
        return $scope;
    }
}
