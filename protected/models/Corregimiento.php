<?php

/**
 * This is the model class for table "corregimiento".
 *
 * The followings are the available columns in table 'corregimiento':
 * @property integer $id
 * @property string $correguimiento
 * @property integer $idmun
 
 *
 * The followings are the available model relations:
 * @property Basicos[] $basicoses
 * @property Municipio $idmun0
 */
class Corregimiento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
     private static $_items=array();
	public function tableName()
	{
		return 'corregimiento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, correguimiento, idmun', 'required'),
			array('id, idmun', 'numerical', 'integerOnly'=>true),
			array('correguimiento', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, correguimiento, idmun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'basicoses' => array(self::HAS_MANY, 'Basicos', 'idcor'),
			'idmun0' => array(self::BELONGS_TO, 'Municipio', 'idmun'),
                    
		);
	}

  
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Codigo',
			'correguimiento' => 'Corregimientos',
			'idmun' => 'Municipio',
			
		);
	}

    public static function ObtenerMunicipio() {
        return CHtml::listData(Municipio::model()->findAll(), 'id', 'municipio');
    }
   
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('correguimiento',$this->correguimiento,true);
		//$criteria->compare('idmun',$this->idmun);
                $criteria->with =array('idmun0');
                 $criteria->addSearchCondition('LOWER(idmun0.municipio)', strtolower($this->idmun));
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public static function items($tipo)
{
 // Devuelve todos los �tems que forman el arreglo
 if(!isset(self::$_items[$tipo]))
  self::loadItems($tipo);
 return self::$_items[$tipo];
}

public static function item($tipo, $id)
{
 // Devuelve el �tem al que le corresponde el id
 if(!isset(self::$_items[$tipo]))
  self::loadItems($tipo);
 return isset(self::$_items[$tipo][$id]) ? self::$_items[$tipo][$id] : false;
}

private static function loadItems($tipo)
{
 // Obtiene los registros
 self::$_items[$tipo]=array();
 $models=self::model()->findAll(array(
  'order'=>'correguimiento',
 ));
 //self::$_items[$tipo][""]=""; // Descomentar para incluir un campo en blanco al inicio, para cuando el campo puede ser nulo
 foreach($models as $model)
  self::$_items[$tipo][$model->id]=$model->correguimiento;
}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Corregimiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
