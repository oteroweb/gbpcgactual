<?php

/**
 * This is the model class for table "pa".
 *
 * The followings are the available columns in table 'pa':
 * @property integer $id
 * @property integer $idpgar
 * @property string $anioinicio
 * @property string $aniofinal
 * @property string $planaccion
 *
 * The followings are the available model relations:
 * @property Lexpc[] $lexpcs
 * @property Pgar $idpgar0
 * @property Programacorporactivo[] $programacorporactivos
 * @property Valorpa[] $valorpas
 */
class Pa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idpgar, anioinicio, aniofinal, planaccion', 'required'),
			array('idpgar', 'numerical', 'integerOnly'=>true),
			array('anioinicio, aniofinal', 'length', 'max'=>4),
			array('planaccion', 'length', 'max'=>250),
			/*
			//Example username
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                 'message'=>'Username can contain only alphanumeric 
                             characters and hyphens(-).'),
          	array('username','unique'),
          	*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idpgar, anioinicio, aniofinal, planaccion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lexpcs' => array(self::HAS_MANY, 'Lexpc', 'idpc'),
			'idpgar0' => array(self::BELONGS_TO, 'Pgar', 'idpgar'),
			'programacorporactivos' => array(self::HAS_MANY, 'Programacorporactivo', 'idpa'),
			'valorpas' => array(self::HAS_MANY, 'Valorpa', 'idpa'),
		);
	}

  public static function Obtenerpgar() {
        return CHtml::listData(Pgar::model()->findAll(), 'id', 'detalle');
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idpgar' => 'Pgar',
			'anioinicio' => 'Anio Inicio',
			'aniofinal' => 'Anio Final',
			'planaccion' => 'Plan de Accion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idpgar',$this->idpgar);
		$criteria->compare('anioinicio',$this->anioinicio,true);
		$criteria->compare('aniofinal',$this->aniofinal,true);
		$criteria->compare('planaccion',$this->planaccion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() 
    {
        $userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
		
		if($this->isNewRecord)
        {           
                        						
        }else{
                        						
        }

        
        return parent::beforeSave();
    }

    public function beforeDelete () {
		$userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
                                
        return false;
    }

    public function afterFind()    {
         
        parent::afterFind();
    }
	
		
	public function defaultScope()
    {
    	/*
    	//Example Scope
    	return array(
	        'condition'=>"deleted IS NULL ",
            'order'=>'create_time DESC',
            'limit'=>5,
        );
        */
        $scope=array();

        
        return $scope;
    }
}
