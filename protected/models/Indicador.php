<?php

/**
 * This is the model class for table "indicador".
 *
 * The followings are the available columns in table 'indicador':
 * @property integer $id
 * @property integer $idact
 * @property integer $idunidad
 * @property string $indicador
 * @property double $lineabase
 * @property integer $total
 * @property integer $ano1
 * @property integer $ano2
 * @property integer $ano3
 * @property integer $ano4
 *
 * The followings are the available model relations:
 * @property Actividada $idact0
 * @property Unidaddemedida $idunidad0
 * @property Meta[] $metas
 */
class Indicador extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'indicador';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idact, total', 'required'),
			array('idact, idunidad, total, ano1, ano2, ano3, ano4', 'numerical', 'integerOnly'=>true),
			array('lineabase', 'numerical'),
			array('indicador', 'length', 'max'=>250),
			/*
			//Example username
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                 'message'=>'Username can contain only alphanumeric 
                             characters and hyphens(-).'),
          	array('username','unique'),
          	*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idact, idunidad, indicador, lineabase, total, ano1, ano2, ano3, ano4', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idact0' => array(self::BELONGS_TO, 'Actividada', 'idact'),
			'idunidad0' => array(self::BELONGS_TO, 'Unidaddemedida', 'idunidad'),
			'metas' => array(self::HAS_MANY, 'Meta', 'idind'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{

if (isset($_GET['id'])){

$actividad = $_GET['id'];
$idpro = Actividada::model()->findByAttributes( array('id'=>$actividad),'idpro');
$idpc = Proyecto::model()->findByAttributes( array('id'=>$idpro['idpro']),'idpc');
$idpa = Programacorporactivo::model()->findByAttributes( array('id'=>$idpc['idpc']),'idpa');
$anioinicio = Pa::model()->findByAttributes( array('id'=>$idpa['idpa']));
$anio=$anioinicio['anioinicio'];
		return array(
			'id' => 'ID',
			'idact' => 'Idact',
			'idunidad' => 'Idunidad',
			'indicador' => 'Indicador',
			'lineabase' => 'Lineabase',
			'total' => 'Total',
			'ano1' => $anio,
			'ano2' => $anio+1,
			'ano3' => $anio+2,
			'ano4' => $anio+3,
		);
}
else  {
	;
		return array(
			'id' => 'ID',
			'idact' => 'Idact',
			'idunidad' => 'Idunidad',
			'indicador' => 'Indicador',
			'lineabase' => 'Lineabase',
			'total' => 'Total',
			'ano1' => '2012',
			'ano2' => '2013',
			'ano3' => '2014',
			'ano4' => '2015',
		);
}

		
	}


 public static function ObtenerInd() {
        return CHtml::listData(Actividada::model()->findAll(), 'id', 'actividad');
    }
	 public static function Obteneruni() {
        return CHtml::listData(Unidaddemedida::model()->findAll(), 'id', 'unidad');
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idact',$this->idact);
		$criteria->compare('idunidad',$this->idunidad);
		$criteria->compare('indicador',$this->indicador,true);
		$criteria->compare('lineabase',$this->lineabase);
		$criteria->compare('total',$this->total);
		$criteria->compare('ano1',$this->ano1);
		$criteria->compare('ano2',$this->ano2);
		$criteria->compare('ano3',$this->ano3);
		$criteria->compare('ano4',$this->ano4);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Indicador the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() 
    {
        $userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
		
		if($this->isNewRecord)
        {           
                        						
        }else{
                        						
        }

        
        return parent::beforeSave();
    }

    public function beforeDelete () {
		$userId=0;
		if(null!=Yii::app()->user->id) $userId=(int)Yii::app()->user->id;
                                
        return false;
    }

    public function afterFind()    {
         
        parent::afterFind();
    }
	
		
	public function defaultScope()
    {
    	/*
    	//Example Scope
    	return array(
	        'condition'=>"deleted IS NULL ",
            'order'=>'create_time DESC',
            'limit'=>5,
        );
        */
        $scope=array();

        
        return $scope;
    }
}
