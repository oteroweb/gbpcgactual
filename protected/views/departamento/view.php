<?php
$this->breadcrumbs = array(
    'Departamentos' => array('index'),
    $model->id,
);

$this->menu = array(
    array(
        'label' => 'Departamentos',
        ',itemOptions' => array('class' => 'nav-header')
    ),
    array('label' => 'Inicio', 'url' => array('admin'), 'icon' => 'list-alt'),
    array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus'),
    array('label' => 'Actualizar', 'url' => array('update', 'id' => $model->id), 'icon' => 'edit'),
    array('label' => 'Borrar', 'url' => '#', 'linkOptions' => array('submit' => array('Borrar', 'id' => $model->id), 'confirm' => 'Desea borrar realmente..?'), 'icon' => 'remove'),
);
?>

<legend> <H3>Departamentos <?php echo $model->id; ?></h3></legend>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'type'=>'striped bordered condensed',
     'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => array(
        'id',
        'departamento',
    ),
));
?>
