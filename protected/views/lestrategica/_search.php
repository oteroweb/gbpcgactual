<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="row-fluid">
    <div class="span3"><?php echo $form->dropDownListRow($model, 'idpgar', Lestrategica::Obtenerpgar(),array('empty' => '')); ?></div>
	<div class="span9"><?php echo $form->textFieldRow($model,'lineaestrategica',array('class'=>'span8','maxlength'=>250)); ?></div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>

</div>
<?php $this->endWidget(); ?>
