<?php
$this->breadcrumbs=array(
	'Municipios'=>array('index'),
	'Administrar',
);

$this->menu=array(
array(
        'label' => 'Municipios',
        'itemOptions' => array('class' => 'nav-header')
    ),
    array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('municipio-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<legend><h3>Municipios</h3></legend>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
     'type' => 'bordered',
'id'=>'municipio-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns' => array(
        'id',
        'municipio',
        array('name' => 'iddep',
            'value' => '$data->iddep0->departamento', 'type' => 'text',),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
             'template'=>'{view}{update}',
        ),
    ),
)); ?>
