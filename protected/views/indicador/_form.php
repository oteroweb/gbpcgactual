<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'indicador-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model,'idact',array('class'=>'span5')); ?>
<?php echo $form->dropDownListRow($model,'idact',Indicador::ObtenerInd(), array('empty' => '')); ?>
<?php // echo $form->textFieldRow($model,'idunidad',array('class'=>'span5')); ?>
<?php echo $form->dropDownListRow($model,'idunidad',Indicador::Obteneruni(), array('empty' => '')); ?>
<?php echo $form->textFieldRow($model,'indicador',array('class'=>'span5','maxlength'=>250)); ?>
<?php echo $form->textFieldRow($model,'lineabase',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'total',array('class'=>'span5')); ?>


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
