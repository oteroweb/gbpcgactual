<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

			<?php echo $form->textFieldRow($model,'idact',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idunidad',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'indicador',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textFieldRow($model,'lineabase',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'total',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
