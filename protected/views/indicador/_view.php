<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idact')); ?>:</b>
	<?php echo CHtml::encode($data->idact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idunidad')); ?>:</b>
	<?php echo CHtml::encode($data->idunidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('indicador')); ?>:</b>
	<?php echo CHtml::encode($data->indicador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lineabase')); ?>:</b>
	<?php echo CHtml::encode($data->lineabase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />


</div>