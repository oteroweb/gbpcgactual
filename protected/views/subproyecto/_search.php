<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

			<?php echo $form->textFieldRow($model,'codigo',array('class'=>'span5')); ?>

		<?php echo $form->datepickerRow($model,'fechaderegistro',
								array(
					                'options' => array(
					                    'language' => 'id',
					                    'format' => 'yyyy-mm-dd', 
					                    'weekStart'=> 1,
					                    'autoclose'=>'true',
					                    'keyboardNavigation'=>true,
					                ), 
					            ),
					            array(
					                'prepend' => '<i class="icon-calendar"></i>'
					            )
			);; ?>

		<?php echo $form->textFieldRow($model,'radicado',array('class'=>'span5')); ?>

		<?php echo $form->datepickerRow($model,'fecharadicado',
								array(
					                'options' => array(
					                    'language' => 'id',
					                    'format' => 'yyyy-mm-dd', 
					                    'weekStart'=> 1,
					                    'autoclose'=>'true',
					                    'keyboardNavigation'=>true,
					                ), 
					            ),
					            array(
					                'prepend' => '<i class="icon-calendar"></i>'
					            )
			);; ?>

		<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textFieldRow($model,'detalle',array('class'=>'span5','maxlength'=>500)); ?>

		<?php echo $form->textFieldRow($model,'objetivogeneral',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'idlocalizacion',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'poblacionben',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idunidad',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'meta',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idrecursos',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idpa',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idpc',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idproy',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idact',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'idformulador',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'valortotal',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'valorsolicitado',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
