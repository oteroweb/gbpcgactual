<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subproyecto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<?php // echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'codigo',array('class'=>'span3')); ?>
<?php echo $form->datepickerRow($model,'fechaderegistro',
								array(
					                'options' => array(
					                    'language' => 'id',
					                    'format' => 'yyyy-mm-dd', 
					                    'weekStart'=> 1,
					                    'autoclose'=>'true',
					                    'keyboardNavigation'=>true,
					                ), 
					            ),
					            array(
					                'prepend' => '<i class="icon-calendar"></i>'
					            )
			);; ?>
<?php echo $form->textFieldRow($model,'radicado',array('class'=>'span')); ?>
<?php echo $form->datepickerRow($model,'fecharadicado',
								array(
					                'options' => array(
					                    'language' => 'id',
					                    'format' => 'yyyy-mm-dd', 
					                    'weekStart'=> 1,
					                    'autoclose'=>'true',
					                    'keyboardNavigation'=>true,
					                ), 
					            ),
					            array(
					                'prepend' => '<i class="icon-calendar"></i>'
					            )
			);; ?>
<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span5','maxlength'=>250)); ?>
<?php echo $form->textFieldRow($model,'detalle',array('class'=>'span5','maxlength'=>500)); ?>
<?php echo $form->textFieldRow($model,'objetivogeneral',array('class'=>'span5','maxlength'=>200)); ?>
   <div class="objespecifico"></div>
  <a id="addobj">Añadir objetivo Especifico</a>

<?php echo $form->textFieldRow($model,'idlocalizacion',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'poblacionben',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idunidad',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'meta',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idrecursos',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idpa',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idpc',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idproy',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'idact',array('class'=>'span5')); ?>
<?php echo $form->dropDownList($model,'idformulador',CHtml::listData(Formulador::model()->findAll(), 'id', 'nombre'),array('class'=>'span5','empty'=>'seleccionar formulador'));
?>
<?php echo CHtml::link('Create formulador', "",  // the link for open the dialog
    array(
        'style'=>'cursor: pointer; text-decoration: underline;',
        'onclick'=>"{formuladorform(); $('#dialogClassroom').dialog('open');}"));?>

<?php echo $form->textFieldRow($model,'valortotal',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'valorsolicitado',array('class'=>'span5')); ?>




<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script>
var i=0;
$(function()  {        
  $("#addobj").click(function(){
    var cycleBlock = '<div id="objespe'+i+'">';
    cycleBlock += '<label for="ObjetivoEspecifico_nomobjespecifico">Objetivo Especifico</label> <input type="text" id="ObjetivoEspecifico_0_nomobjespecifico" name="Objespecificos['+i+'][nomobjespecifico]">';
    cycleBlock += '<label for="ObjetivoEspecifico_producto">Producto</label> <input type="text" id="ObjetivoEspecifico_0_producto" name="Objespecificos['+i+'][producto]" class="attrName'+i+'" maxlength="100" size="44">';
    cycleBlock +='<div class="actividad">';
    cycleBlock += '<label for="ObjetivoEspecifico_nomobjespecifico">Actividades</label>';
    cycleBlock +='</div>';
    cycleBlock += '<a id="addact'+i+'">Añadir actividad</a>';
    cycleBlock += '</div>';
    var $cycleBlock = $(cycleBlock);
    $('.objespecifico').append($cycleBlock);
    ac=i;


      $(function(){ 
        var es=1;
        var  activi='#addact'+i;
        console.log(activi);

        
          es=0;
        $(activi).click(function(){
          act=$(this).parent(0);
          idact=$(act).attr("id");
          sub=idact.substr(7,1)
          var activity = '<div id="activi">';
          activity += '<label for="actividad_nomobjespecifico">actividades</label> <input type="text" id="ObjetivoEspecifico_0_actividad" name="Objespecificos['+sub+'][actividad]['+es+']">';
          activity += '</div>';
          $(act).append(activity);
          es++;
        });
      });



  i++;
  });
});



</script>











 
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogClassroom',
    'options'=>array(
        'title'=>'Create classroom',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>550,
        'height'=>470,
    ),
));?>
<div class="divForForm">
	
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'formulador-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($formulador); ?>

<?php echo $form->textFieldRow($formulador,'id',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($formulador,'idtlusuario',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($formulador,'tipodocumento',array('class'=>'span5','maxlength'=>30)); ?>
<?php echo $form->textFieldRow($formulador,'documento',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($formulador,'nombre',array('class'=>'span5','maxlength'=>100)); ?>
<?php echo $form->textFieldRow($formulador,'email',array('class'=>'span5','maxlength'=>100)); ?>


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$formulador->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

</div>
 
<?php $this->endWidget();?>
 
<script type="text/javascript">
// here is the magic
function formuladorform()
{
    <?php echo CHtml::ajax(array(
            'url'=>array('formulador/create'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogClassroom div.divForForm').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogClassroom div.divForForm form').submit(formuladorform);
                }
                else
                {
                    $('#dialogClassroom div.divForForm').html(data.div);
                    setTimeout(\"$('#dialogClassroom').dialog('close') \",3000);
                }
 
            } ",
            ))?>;
    return false; 
 
}
 
</script>