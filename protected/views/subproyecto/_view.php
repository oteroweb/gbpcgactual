<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaderegistro')); ?>:</b>
	<?php echo CHtml::encode($data->fechaderegistro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('radicado')); ?>:</b>
	<?php echo CHtml::encode($data->radicado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecharadicado')); ?>:</b>
	<?php echo CHtml::encode($data->fecharadicado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detalle')); ?>:</b>
	<?php echo CHtml::encode($data->detalle); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('objetivogeneral')); ?>:</b>
	<?php echo CHtml::encode($data->objetivogeneral); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idlocalizacion')); ?>:</b>
	<?php echo CHtml::encode($data->idlocalizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poblacionben')); ?>:</b>
	<?php echo CHtml::encode($data->poblacionben); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idunidad')); ?>:</b>
	<?php echo CHtml::encode($data->idunidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta')); ?>:</b>
	<?php echo CHtml::encode($data->meta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idrecursos')); ?>:</b>
	<?php echo CHtml::encode($data->idrecursos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpa')); ?>:</b>
	<?php echo CHtml::encode($data->idpa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpc')); ?>:</b>
	<?php echo CHtml::encode($data->idpc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idproy')); ?>:</b>
	<?php echo CHtml::encode($data->idproy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idact')); ?>:</b>
	<?php echo CHtml::encode($data->idact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idformulador')); ?>:</b>
	<?php echo CHtml::encode($data->idformulador); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valortotal')); ?>:</b>
	<?php echo CHtml::encode($data->valortotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valorsolicitado')); ?>:</b>
	<?php echo CHtml::encode($data->valorsolicitado); ?>
	<br />

	*/ ?>

</div>