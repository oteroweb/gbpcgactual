<?php
/* @var $this ActividadaController */
/* @var $model Actividada */

$this->breadcrumbs=array(
	'Actividadas'=>array('index'),
	$model->id,
);

$menu=array();
require(dirname(__FILE__).DIRECTORY_SEPARATOR.'_menu.php');


$menu2=array(
		array('label'=>'Crear','url'=>array('create'),'icon'=>'plus')	
);

if(!isset($_GET['asModal'])){
?>
<?php $box = $this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
        'title' => 'View Actividadas #'.$model->id,
        'headerIcon' => 'icon- fa fa-eye',
        'headerButtons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButtonGroup',
                'type' => 'success',
                // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons' => $menu2
            ),
        ) 
    )
);?>
<?php
}
?>

		<?php $this->widget('bootstrap.widgets.TbAlert', array(
		    'block'=>false, // display a larger alert block?
		    'fade'=>true, // use transitions?
		    'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
		    'alerts'=>array( // configurations per alert type
		        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		    ),
		));
		?>		
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
			'id',
		'idpro',
		'codact',
		'actividad',
		/*
		//CONTOH
		array(
	        'header' => 'Level',
	        'name'=> 'ref_level_id',
	        'type'=>'raw',
	        'value' => ($model->Level->name),
	        // 'value' => ($model->status)?"on":"off",
	        // 'value' => @Admin::model()->findByPk($model->createdBy)->username,
	    ),

	    */
	),
)); ?>

<?php
if(!isset($_GET['asModal'])){
	$this->endWidget();}
?> 
<?php  
   $this->widget(
        'booster.widgets.TbGridView',
        array(
            'type' => 'striped bordered',
            'dataProvider' => $indicador,
            'columns' => array(
            	
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'indicador', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'lineabase', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano1', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada', )
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano2', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

		array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano3', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
		                ),

array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano4', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),


array( 'class' => 'booster.widgets.TbEditableColumn', 'name' => 'total', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

            ),
        )
    );

?>


