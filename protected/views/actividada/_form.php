<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'actividada-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	// 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


<?php echo $form->dropDownListRow($model,'idpro',Actividada::ObtenerActividad(), array('empty' => '')); ?>
<?php echo $form->textFieldRow($model,'codact',array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'actividad',array('class'=>'span5','maxlength'=>250)); ?>
<div id="valores">
	
	<h1>VALORES</h1>
<table>
<tr>

<?php 
$añoinicio=2012;
$añofinal=2015;
for ($i=$añoinicio; $i <= $añofinal; $i++) { 
?>	
<td><?php echo $i; ?></td>

<?php } ?>	
</tr>
<tr>
<?php // for ($i=$añoinicio; $i <= $añofinal; $i++) { 
	?>
<td>
<?php echo $form->textFieldRow($model,'valor1',array('class'=>'span5')); ?>
</td>
<td>
<?php echo $form->textFieldRow($model,'valor2',array('class'=>'span5')); ?>
</td>
<td>
<?php echo $form->textFieldRow($model,'varlor3',array('class'=>'span5')); ?>
</td>
<td>
<?php echo $form->textFieldRow($model,'valor4',array('class'=>'span5')); ?>
</td>

<?php  // } ?>	

</tr>
</table>
</div>
<div id="indicadores">
	
<h1>INDICADORES</h1>
<a id="masindicador">añadir indicador</a>
<br>
<table>
	<tr>
		<td width="22%">
			<label style="display:inline" for="Indicador_'+num+'_indicador" class="required">Indicador 
<span class="required">*</span>
</label>
		</td>
		<td width="25%">
		<label style="display:inline" for="Indicador_0_lineabase" class="required">Lineabase <span class="required">*</span>
		</td>
		<td width="7%">
		<label style="display:inline" for="Indicador_0_lineabase" class="required">U. Medida <span class="required">*</span>
		</td>
		<td>
			Años
		</td>
	</tr>

	<tr>
		<td id="colindicador">
		<input class="" maxlength="250" placeholder="[0]indicador" name="Indicador[0][indicador]" id="Indicador_0_indicador" type="text">
		</td>
		<td id="collineabase">
	<input class="" style="width:203px" placeholder="[0]lineabase" name="Indicador[0][lineabase]" id="Indicador_0_lineabase" type="text">
		</td>

<td id="colunidadmedida">
<select placeholder="Idunidad" name="Indicador[0][unidadmedida]" id="Indicador_idunidad">
<option value=""></option><option value="1">pesos</option><option value="2">porcentaje</option><option value="3">numero de viviendas</option><option value="4">ADMON Costos A.I.U</option><option value="5">Amperios hora</option><option value="6">Pesos por año</option><option value="7">Pesos por beneficiar</option><option value="8">camas</option><option value="9">capacidad</option><option value="10">cartilla</option><option value="11">numero de conexiones</option><option value="12">pesos por consumo</option><option value="13">contrato</option><option value="14">cupos</option><option value="15">Dia</option><option value="16">Equipos</option><option value="17">eventos</option><option value="18">Familias Beneficiada</option><option value="19">Galon</option><option value="20">global</option><option value="21">Pesos por hectárea </option><option value="22">Hectárea </option><option value="23">Hectáreas por año</option><option value="24">Numero de Habitantes</option><option value="25">Hectáreas por munici</option><option value="26">hora</option><option value="27">Hora Maquina </option><option value="28">Caballos de fuerza</option><option value="29">Horas al dia</option><option value="30">Hombres al mes</option><option value="31">inf. diag. por año</option><option value="32">informe por año</option><option value="33">INTV interventoria </option><option value="34">Jornal</option><option value="35">Juego</option><option value="36">kilogramo</option><option value="37">KIT</option><option value="38">Pesos por kilómetro </option><option value="39">Kilómetros </option><option value="40">Pesos por km por año</option><option value="41">kilómetro por hora </option><option value="42">kilómetro via</option><option value="43">kilómetro cuadrado</option><option value="44">pesos por kilo volti</option><option value="45">Kilo Voltio Amperio</option><option value="46">Kilovatio</option><option value="47">Litros por segundo</option><option value="48">Pesos por metro</option><option value="49">Metros</option><option value="50">mes</option><option value="51">Minutos</option><option value="52">Minas/año</option><option value="53">Metro lineal</option><option value="54">Mega voltio amperio</option><option value="55">Metro cuadrado</option><option value="56">Pesos por metro cubi</option><option value="57">Metro cubico</option><option value="58">Metro Cubico por año</option><option value="59">metro cubico sobre k</option><option value="60">N. Puestos Dotados</option><option value="61">Numero</option><option value="62">Numero de Barrios</option><option value="63">Numero de Fincas</option><option value="64">Numero de municipios</option><option value="65">Numero de veredas</option><option value="66">pers. capaci por año</option><option value="67">Personas</option><option value="68">Pesos por habitante</option><option value="69">Planes mejor por año</option><option value="70">Proyectos</option><option value="71">Punto</option><option value="72">Rollo</option><option value="73">Suscriptores</option><option value="74">Talleres</option><option value="75">Tonelada-Metro</option><option value="76">Tonelada</option><option value="77">Toneladas al año</option><option value="78">Unidad</option><option value="79">usuarios</option><option value="80">N. vehículos por dia</option><option value="81">Viaje</option><option value="82">Pesos por vivienda</option><option value="83">Vatios pico</option><option value="84">Hectometros cúbicos </option></select>
	</td>
		<td id="colano">
		<input name="Indicador[0][ano1]" style="width:20%" placeholder="año" type="text">
<input name="Indicador[0][ano2]" style="width:20%" placeholder="año" type="text">
<input name="Indicador[0][ano3]" style="width:20%" placeholder="año" type="text">
<input name="Indicador[0][ano4]" style="width:20%" placeholder="año" type="text"> </br>

		</td>
	</tr>
</table>




</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<?php  
   $this->widget(
        'booster.widgets.TbGridView',
        array(
            'type' => 'striped bordered',
            'dataProvider' => $indicador,
            'columns' => array(
            	
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'indicador', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'lineabase', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano1', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada', )
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano2', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

		array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano3', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
		                ),

array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano4', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),


array( 'class' => 'booster.widgets.TbEditableColumn', 'name' => 'total', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

            ),
        )
    );

?>



<script>
var num = 1;
$("#masindicador").click(function(){ 
	// Indicador[0][indicador]
var indicador = '<input class="" placeholder="['+num+'][indicador]" name="Indicador['+num+'][indicador]" id="Indicador_'+num+'_indicador" type="text">';
 var lineabase = '<input class="" placeholder="['+num+'][lineabase]" name="Indicador['+num+'][lineabase]" id="Indicador_'+num+'_lineabase" type="text">';
 var unidadmedida = '<select placeholder="Idunidad" name="Indicador['+num+'][idunidad]" id="Indicador_idunidad"><option value=""></option><option value="1">pesos</option><option value="2">porcentaje</option><option value="3">numero de viviendas</option><option value="4">ADMON Costos A.I.U</option><option value="5">Amperios hora</option><option value="6">Pesos por año</option><option value="7">Pesos por beneficiar</option><option value="8">camas</option><option value="9">capacidad</option><option value="10">cartilla</option><option value="11">numero de conexiones</option><option value="12">pesos por consumo</option><option value="13">contrato</option><option value="14">cupos</option><option value="15">Dia</option><option value="16">Equipos</option><option value="17">eventos</option><option value="18">Familias Beneficiada</option><option value="19">Galon</option><option value="20">global</option><option value="21">Pesos por hectárea </option><option value="22">Hectárea </option><option value="23">Hectáreas por año</option><option value="24">Numero de Habitantes</option><option value="25">Hectáreas por munici</option><option value="26">hora</option><option value="27">Hora Maquina </option><option value="28">Caballos de fuerza</option><option value="29">Horas al dia</option><option value="30">Hombres al mes</option><option value="31">inf. diag. por año</option><option value="32">informe por año</option><option value="33">INTV interventoria </option><option value="34">Jornal</option><option value="35">Juego</option><option value="36">kilogramo</option><option value="37">KIT</option><option value="38">Pesos por kilómetro </option><option value="39">Kilómetros </option><option value="40">Pesos por km por año</option><option value="41">kilómetro por hora </option><option value="42">kilómetro via</option><option value="43">kilómetro cuadrado</option><option value="44">pesos por kilo volti</option><option value="45">Kilo Voltio Amperio</option><option value="46">Kilovatio</option><option value="47">Litros por segundo</option><option value="48">Pesos por metro</option><option value="49">Metros</option><option value="50">mes</option><option value="51">Minutos</option><option value="52">Minas/año</option><option value="53">Metro lineal</option><option value="54">Mega voltio amperio</option><option value="55">Metro cuadrado</option><option value="56">Pesos por metro cubi</option><option value="57">Metro cubico</option><option value="58">Metro Cubico por año</option><option value="59">metro cubico sobre k</option><option value="60">N. Puestos Dotados</option><option value="61">Numero</option><option value="62">Numero de Barrios</option><option value="63">Numero de Fincas</option><option value="64">Numero de municipios</option><option value="65">Numero de veredas</option><option value="66">pers. capaci por año</option><option value="67">Personas</option><option value="68">Pesos por habitante</option><option value="69">Planes mejor por año</option><option value="70">Proyectos</option><option value="71">Punto</option><option value="72">Rollo</option><option value="73">Suscriptores</option><option value="74">Talleres</option><option value="75">Tonelada-Metro</option><option value="76">Tonelada</option><option value="77">Toneladas al año</option><option value="78">Unidad</option><option value="79">usuarios</option><option value="80">N. vehículos por dia</option><option value="81">Viaje</option><option value="82">Pesos por vivienda</option><option value="83">Vatios pico</option><option value="84">Hectometros cúbicos </option></select>';
 var ano ='<input name="Indicador['+num+'][ano1]" style="width:20%" placeholder="año" type="text">';
ano +=    '<input name="Indicador['+num+'][ano2]" style="width:20%" placeholder="año" type="text">';
ano +=    '<input name="Indicador['+num+'][ano3]" style="width:20%" placeholder="año" type="text">';
ano +=    '<input name="Indicador['+num+'][ano4]" style="width:20%" placeholder="año" type="text"></br>';

$('#colindicador').append(indicador);
$('#collineabase').append(lineabase);
$('#colano').append(ano);
$('#colunidadmedida').append(unidadmedida);
console.log(indicador);
num++;
});
</script>



