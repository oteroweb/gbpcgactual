<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpro')); ?>:</b>
	<?php echo CHtml::encode($data->idpro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codact')); ?>:</b>
	<?php echo CHtml::encode($data->codact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actividad')); ?>:</b>
	<?php echo CHtml::encode($data->actividad); ?>
	<br />

	    <? $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'usergrid',
        'itemsCssClass' => 'table-bordered items',
        'dataProvider' => $dataProvider,
        'columns'=>array(
            array(
               'class' => 'editable.EditableColumn',
               'name' => 'user_name',
               'headerHtmlOptions' => array('style' => 'width: 110px'),
               'editable' => array(    //editable section
                      'apply'      => '$data->user_status != 4', //can't edit deleted users
                      'url'        => $this->createUrl('site/updateUser'),
                      'placement'  => 'right',
                  )               
            ),
            
            array( 
                  'class' => 'editable.EditableColumn',
                  'name' => 'user_status',
                  'headerHtmlOptions' => array('style' => 'width: 100px'),
                  'editable' => array(
                      'type'     => 'select',
                      'url'      => $this->createUrl('site/updateUser'),
                      'source'   => $this->createUrl('site/getStatuses'),
                      'options'  => array(    //custom display 
                         'display' => 'js: function(value, sourceData) {
                              var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                                  colors = {1: "green", 2: "blue", 3: "red", 4: "gray"};
                              $(this).text(selected[0].text).css("color", colors[value]);    
                          }'
                      ),
                     //onsave event handler 
                     'onSave' => 'js: function(e, params) {
                          console && console.log("saved value: "+params.newValue);
                     }',
                     //source url can depend on some parameters, then use js function:
                     /*
                     'source' => 'js: function() {
                          var dob = $(this).closest("td").next().find(".editable").text();
                          var username = $(this).data("username");
                          return "?r=site/getStatuses&user="+username+"&dob="+dob;
                     }',
                     'htmlOptions' => array(
                         'data-username' => '$data->user_name'
                     )
                     */
                  )
             ),
          
             array( 
                  'class' => 'editable.EditableColumn',
                  'name'  => 'user_dob',
                  'headerHtmlOptions' => array('style' => 'width: 100px'),
                  'editable' => array(
                      'type'          => 'date',
                      'viewformat'    => 'dd.mm.yyyy',
                      'url'           => $this->createUrl('site/updateUser'),
                      'placement'     => 'right',
                  )
             ), 
             
             array( 
                'class' => 'editable.EditableColumn',
                'name' => 'user_comment',
                'editable' => array(
                    'type'      => 'textarea',
                    'url'       => $this->createUrl('site/updateUser'),
                    'placement' => 'left',
                )
              ),  
             
             //editable related attribute with sorting.
             //see http://www.yiiframework.com/wiki/281/searching-and-sorting-by-related-model-in-cgridview  
             array( 
                'class' => 'editable.EditableColumn',
                'name' => 'virtual_field',
                'value' => 'CHtml::value($data, "profile.language")',
                'editable' => array(
                    'type'      => 'text',
                    'attribute' => 'profile.language',
                    'url'       => $this->createUrl('site/updateProfile'),
                    'placement' => 'left',
                )
            ), 
        ),
    )); 
    ?>
<?php  
   $this->widget(
        'booster.widgets.TbGridView',
        array(
            'type' => 'striped bordered',
            'dataProvider' => $indicador,
            'columns' => array(
              
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'indicador', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'lineabase', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano1', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada', )
                ),
                 array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano2', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

    array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano3', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                    ),

array( 'class' => 'booster.widgets.TbEditableColumn',  'name' => 'ano4', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),


array( 'class' => 'booster.widgets.TbEditableColumn', 'name' => 'total', 'sortable' => false, 'editable' => array( 'url' => $this->createUrl('actividada/updateajax'),'placement' => 'right','inputclass' => 'span3', 'emptytext'=> 'nada')
                ),

            ),
        )
    );

?>




</div>