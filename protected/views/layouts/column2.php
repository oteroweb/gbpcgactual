<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
    <div class="span2">
        <?php
            $this->widget(
    'bootstrap.widgets.TbMenu',
    array(
    'type' => 'list',
    'items' => $this->menu,
    )
    );
        ?>
       
    </div><!-- sidebar span3 -->

    <div class="span9">
        <div class="main">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>