 <?php
       $this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => Yii::t('Admin', 'Administ'), 'url' => $this->createUrl('/admin/'), 'icon' => 'home',
		),
		array(
			'label' => 'User', 'items' => array(
				array('label' => 'Manage', 'url' => '#', 'icon' => 'user'),
				array('label' => 'Groups', 'url' => '#', 'icon' => 'th'),
			),
		),
		array(
			'label' => 'Top Secret', 'access' => 'administrador', 'items' => array(
				array('label' => 'Can you see?', 'url' => '#', 'icon' => 'lock'),
			),
		),
		array(
			'label' => 'Finances', 'items' => array(
				array('label' => 'Statistic', 'url' => '#', 'icon' => 'signal'),
				array('label' => 'Billing', 'url' => '#', 'icon' => 'shopping-cart'),
				array('label' => 'Voucher', 'url' => '#', 'icon' => 'tag'),
			),
		),
	),
));
        ?>

