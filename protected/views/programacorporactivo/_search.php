<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

			<?php echo $form->textFieldRow($model,'codigo',array('class'=>'span5','maxlength'=>20)); ?>
		<?php echo $form->textFieldRow($model,'programacorporactivo',array('class'=>'span5','maxlength'=>500)); ?>

		<?php echo $form->textFieldRow($model,'idpa',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
