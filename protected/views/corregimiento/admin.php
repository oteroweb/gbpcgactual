<?php
$this->breadcrumbs=array(
	'Corregimientos'=>array('index'),
	'Administrar',
);

$this->menu=array(
array(
        'label' => 'Corregimientos',
        'itemOptions' => array('class' => 'nav-header')
    ),
    array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('corregimiento-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<legend><h3>Corregimientos</h3></legend>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
     'type' => 'bordered',
'id'=>'corregimiento-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns' => array(
    
        'id',
        'correguimiento',
        array('name' => 'idmun',
            'value' => '$data->idmun0->municipio', 'type' => 'text',),
        
       
        
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
             'template'=>'{view}{update}',
        ),
    ),
)); ?>
