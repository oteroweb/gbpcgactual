<?php
$this->breadcrumbs=array(
	'Corregimientos'=>array('index'),
	'Crear',
);

$this->menu=array(
array(
        'label' => 'Corregimientos',
        'itemOptions' => array('class' => 'nav-header')
    ),
     array('label'=>'Volver Atras', 'url'=>array('admin'),'icon' => 'arrow-left'),
);
?>

<legend><h3>Crear</h3></legend>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>