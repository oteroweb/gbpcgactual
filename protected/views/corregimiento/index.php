<?php
$this->breadcrumbs=array(
	'Corregimientos',
);

$this->menu=array(
array('label'=>'Create Corregimiento','url'=>array('create')),
array('label'=>'Manage Corregimiento','url'=>array('admin')),
);
?>

<h1>Corregimientos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
