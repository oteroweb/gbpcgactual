<?php
$this->breadcrumbs = array(
    'Corregimientos' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Actualizar',
);

$this->menu = array(
    array(
        'label' => 'Corregimientos',
        'itemOptions' => array('class' => 'nav-header')
    ),
    array('label' => 'Inicio', 'url' => array('admin'), 'icon' => 'list-alt'),
    array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus'),
    array('label' => 'Volver Atras', 'url' => array('view', 'id' => $model->id), 'icon' => 'arrow-left'),
);
?>

<legend> <H3>Actualizando Corregimientos <?php echo $model->id; ?></h3></legend>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>