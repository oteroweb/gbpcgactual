<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'corregimiento-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

<?php echo $form->errorSummary($model); ?>

        <?php //echo $form->textFieldRow($model,'idmun',array('class'=>'span5')); ?>
       <?php echo $form->dropDownListRow($model,'idmun', Corregimiento::ObtenerMunicipio(), array('empty' => ' ')); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'correguimiento',array('class'=>'span5','maxlength'=>30)); ?>

	

<div class="form">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
