<?php
/* @var $this BasicosController */
/* @var $model Basicos */

$this->breadcrumbs=array(
	'Basicoses'=>array('index'),
	'Crear',
);

$menu=array();

require(dirname(__FILE__).DIRECTORY_SEPARATOR.'_menu.php');
$this->menu=array(
	
    array('label' => 'Listado de Eventos', 'url' => array('admin'), 'icon' => 'list-alt',
         'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
    
    array('label' => 'Listado de Eventos', 'url' => array('admin2'), 'icon' => 'list-alt',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')),
    
);
?>

<?php $box = $this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
        'title' => '<h3>Informacion Basica</h3>' ,
        'headerIcon' => 'icon-list-alt',
        'headerButtons' => array(
        	array(
            	'class' => 'bootstrap.widgets.TbButtonGroup',
            	'type' => 'success',
            	// '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            	'buttons' => $this->menu
            )
        )        
    )
);?>
		<?php $this->widget('bootstrap.widgets.TbAlert', array(
		    'block'=>false, // display a larger alert block?
		    'fade'=>true, // use transitions?
		    'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
		    'alerts'=>array( // configurations per alert type
		        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		    ),
		));
		?>		
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php $this->endWidget(); ?>