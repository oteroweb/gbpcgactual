<?php
/* @var $this BasicosController */
/* @var $model Basicos */

$this->breadcrumbs=array(
	'Basicoses'=>array('index'),
	'Administrar',
);

$menu=array();
require(dirname(__FILE__).DIRECTORY_SEPARATOR.'_menu.php');
$this->menu=array(
	array('label' => 'Crear', 'url' => array('create2'), 'icon' => 'plus'),	
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#basicos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $box = $this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
        'title' => '<h3>Administrador de Eventos</h3>',
        'headerIcon' => 'icon-list-alt',
        'headerButtons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButtonGroup',
                'type' => 'success',
                // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons' => $this->menu
            ),
        ) 
    )
);?>		<?php $this->widget('bootstrap.widgets.TbAlert', array(
		    'block'=>false, // display a larger alert block?
		    'fade'=>true, // use transitions?
		    'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
		    'alerts'=>array( // configurations per alert type
		        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		    ),
		));
		?>


<?php echo CHtml::link('Buscar Evento','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search2',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

      

<?php echo CHtml::beginForm(array('export2')); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView',array(
	'id'=>'basicos-grid',
	'dataProvider'=>$model->search2(),
	'filter'=>$model,
	'type' => 'bordered condensed', //bordered condensed
	'columns'=>array(
		array('header'=>'No','value'=>'($this->grid->dataProvider->pagination->currentPage*
					 $this->grid->dataProvider->pagination->pageSize
					)+ ($row+1)',
				'htmlOptions' => array('style' =>'width: 25px; text-align:center;'),
		),
			array(
		        'header' => 'Fecha',
		        'name'=> 'fecha',
		        'type'=>'raw',
		        'value' => '(date( " Y-m-d ", strtotime($data->fecha)))',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
	            'htmlOptions' => array('style' => 'text-align:center;'),
			/*	'editable' => array(
					'type'          => 'date',
					'format'		=> 'yyyy-mm-dd', //sent to server
                  	'viewformat'    => 'dd-M-yyyy', //view user
					'url'     => $this->createUrl('editable'),
					'placement'     => 'right',
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			
			array(
		        'header' => 'Codigo',
		        'name'=> 'codevento',
		        'type'=>'raw',
		        'value' => '($data->codevento)',
		      //  'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				/*'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			
			array(
		        'header' => 'Municipio',
		        'name'=> 'idmun',
		        'type'=>'raw',
		        'value' => '($data->idmun0->municipio)',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			
			array(
		        'header' => 'Corregimiento',
		        'name'=> 'idcor',
		        'type'=>'raw',
		        'value' => '($data->idcor0->correguimiento)',
		      //  'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			
		/*	array(
		        'header' => 'Cordx',
		        'name'=> 'cordx',
		        'type'=>'raw',
		        'value' => '($data->cordx)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
			array(
		        'header' => 'Cordy',
		        'name'=> 'cordy',
		        'type'=>'raw',
		        'value' => '($data->cordy)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ), */
			
/*
			array(
		        'header' => 'Cordz',
		        'name'=> 'cordz',
		        'type'=>'raw',
		        'value' => '($data->cordz)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/

			array(
		        'header' => 'Dias',
		        'name'=> 'dia',
		        'type'=>'raw',
		        'value' => '($data->dia)',
		      //  'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				) */
		    ),
			


			array(
		        'header' => 'Evento',
		        'name'=> 'idevento',
		        'type'=>'raw',
		        'value' => '($data->idevento0->evento)',
		     //   'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			


			array(
		        'header' => 'Calificacion del Evento',
		        'name'=> 'cal_evento',
		        'type'=>'raw',
		        'value' => '($data->cal_evento)',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			


			array(
		        'header' => 'Causa del Evento',
		        'name'=> 'causa_evento',
		        'type'=>'raw',
		        'value' => '($data->causa_evento)',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			


			array(
		        'header' => 'Vulnerabilidad',
		        'name'=> 'vulnerabilidad_id',
		        'type'=>'raw',
		        'value' => '($data->vulnerabilidad->vulnerabiliad)',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			

/*
			array(
		        'header' => 'Gradovul',
		        'name'=> 'gradovul',
		        'type'=>'raw',
		        'value' => '($data->gradovul)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Muertos',
		        'name'=> 'Muertos',
		        'type'=>'raw',
		        'value' => '($data->Muertos)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Heridos',
		        'name'=> 'heridos',
		        'type'=>'raw',
		        'value' => '($data->heridos)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Damnificados',
		        'name'=> 'damnificados',
		        'type'=>'raw',
		        'value' => '($data->damnificados)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Enfermos',
		        'name'=> 'enfermos',
		        'type'=>'raw',
		        'value' => '($data->enfermos)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Evacuados',
		        'name'=> 'evacuados',
		        'type'=>'raw',
		        'value' => '($data->evacuados)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Desaparecidos',
		        'name'=> 'desaparecidos',
		        'type'=>'raw',
		        'value' => '($data->desaparecidos)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Reubicados',
		        'name'=> 'reubicados',
		        'type'=>'raw',
		        'value' => '($data->reubicados)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Vdestruidas',
		        'name'=> 'vdestruidas',
		        'type'=>'raw',
		        'value' => '($data->vdestruidas)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Vafectadas',
		        'name'=> 'vafectadas',
		        'type'=>'raw',
		        'value' => '($data->vafectadas)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Semovientes',
		        'name'=> 'semovientes',
		        'type'=>'raw',
		        'value' => '($data->semovientes)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Cultivos',
		        'name'=> 'cultivos',
		        'type'=>'raw',
		        'value' => '($data->cultivos)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Viasafectadas',
		        'name'=> 'viasafectadas',
		        'type'=>'raw',
		        'value' => '($data->viasafectadas)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Bosque',
		        'name'=> 'bosque',
		        'type'=>'raw',
		        'value' => '($data->bosque)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Ceducativo',
		        'name'=> 'ceducativo',
		        'type'=>'raw',
		        'value' => '($data->ceducativo)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Chospital',
		        'name'=> 'chospital',
		        'type'=>'raw',
		        'value' => '($data->chospital)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Valorperdida',
		        'name'=> 'valorperdida',
		        'type'=>'raw',
		        'value' => '($data->valorperdida)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/

			array(
		        'header' => 'Fuente de informacion',
		        'name'=> 'finformacion',
		        'type'=>'raw',
		        'value' => '($data->finformacion)',
		       // 'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
			/*	'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)*/
		    ),
			

/*
			array(
		        'header' => 'Descripcion',
		        'name'=> 'descripcion',
		        'type'=>'raw',
		        'value' => '($data->descripcion)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Accionespar',
		        'name'=> 'accionespar',
		        'type'=>'raw',
		        'value' => '($data->accionespar)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Observaciones',
		        'name'=> 'observaciones',
		        'type'=>'raw',
		        'value' => '($data->observaciones)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Imagen1',
		        'name'=> 'imagen1',
		        'type'=>'raw',
		        'value' => '($data->imagen1)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Imagen2',
		        'name'=> 'imagen2',
		        'type'=>'raw',
		        'value' => '($data->imagen2)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Imagen3',
		        'name'=> 'imagen3',
		        'type'=>'raw',
		        'value' => '($data->imagen3)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
/*
			array(
		        'header' => 'Pdf',
		        'name'=> 'pdf',
		        'type'=>'raw',
		        'value' => '($data->pdf)',
		        'class' => 'bootstrap.widgets.TbEditableColumn',
	            'headerHtmlOptions' => array('style' => 'text-align:center'),
				'editable' => array(
					'type'    => 'textarea',
					'url'     => $this->createUrl('editable'),
					'params' => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
				)
		    ),
			
*/
		/*
		//Contoh
		array(
	        'header' => 'Level',
	        'name'=> 'ref_level_id',
	        'type'=>'raw',
	        'value' => '($data->Level->name)',
	        // 'value' => '($data->status)?"on":"off"',
	    ),
	    */
	    array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>'{view}{update}',
			'buttons'=>array
            (
                'view' => array
                (    
                	'url' => '$data->id',              
                	'click' => 'function(){
                		data=$(this).attr("href").split("|")
                		$("#myModalHeader").html(data[1]);
	        			$("#myModalBody").load("'.$this->createUrl('view').'&id="+data[0]+"&asModal=true");
                		$("#myModal").modal();
                		return false;
                	}', 
                ),
                            
                             'update' => array
                (    
                	'url'=>"CHtml::normalizeUrl(array('update2', 'id'=>\$data->id))",
                ),
                            
            )
		),
	),
    
       'extendedSummary' => array(
    'title' => 'Eventos',
    'columns' => array(
                       'idevento' => array(
                                           'label'=>'Total Eventos',
                                           'types' => array(
                                                            'INUNDACION'=>array('label'=>'INUNDACION'),
                                                            'VENDAVAL'=>array('label'=>'VENDAVAL'),
                                                            'LLUVIA'=>array('label'=>'LLUVIA'),
                                                            'SEQUIA'=>array('label'=>'SEQUIA'),
                                                            'DESLIZAMIENTO'=>array('label'=>'DESLIZAMIENTO'),
                                                            'AVALANCHA'=>array('label'=>'AVALANCHA'),
                                                            'COLAPSO'=>array('label'=>'COLAPSO'),
                                                            'HELADA'=>array('label'=>'HELADA'),
                                                            'HURACAN'=>array('label'=>'HURACAN'),
                                                            'INCENDIO FORESTAL'=>array('label'=>'INCENDIO FORESTAL'),
                                                            'TORMENTA ELECTRICA'=>array('label'=>'TORMENTA ELECTRICA'),
                                                            'TORMENTA TROPICAL'=>array('label'=>'TORMENTA TROPICAL'),
                                                            'TSUNAMI'=>array('label'=>'TSUNAMI'),
                                                            'SISMO'=>array('label'=>'SISMO'),
                                                            'REMOCION EN MASA'=>array('label'=>'REMOCION EN MASA'),
                                                            ),
                                             'class'=>'TbPercentOfTypeGooglePieOperation',
                                            )
                        )
                                ),
    'extendedSummaryOptions' => array(
                                      'class' => 'well pull-right',
                                       'style' => 'width:550px'
                                      ),
    
)); ?>









<select name="fileType" style="width:170px;">
	<option value="Excel5">EXCEL 97-2003 (xls)</option>
	<option value="Excel2007">EXCEL 2007-2013 (xlsx)</option>
	<option value="HTML">HTML</option> 
	<option value="PDF">PDF</option>  
	<option value="WORD">WORD (docx)</option> 
</select>
<br>

<?php 
$this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit', 'icon'=>'icon-download','label'=>'Exportar', 'type'=> 'primary'));
?>
<?php echo CHtml::endForm(); ?>

<?php /*
$box = $this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
        'title' => 'Import Data',
        'htmlOptions' => array('style' => 'width:25%; text-align:center;margin-top:-100px', 'class'=>'pull-right'),
    )
);?>
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'import-admin-form',
		'type' => 'inline',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array(
			'enctype'=>'multipart/form-data',
		),
		'action' => $this->createUrl('import'),  //<- your form action here
	)); ?>
	<?php echo $form->fileFieldRow($model,'fileImport'); ?> 
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>'Import',
		'icon'=>'fa fa-download'
	)); ?>
	<br>
	(file type permitted: xls, xlsx, ods only)
	<?php $this->endWidget(); ?>
<?php $this->endWidget();     */  ?>
        
        
        

<?php $this->endWidget();    ?>
               
<?php  $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4 id="myModalHeader">Detalles del Evento</h4>
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>Cargando...</p>
    </div>
 
    <div class="modal-footer">
        <?php  $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Cerrar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
        
    </div>
 
<?php  $this->endWidget(); ?>



<?php
        $data = $model->search()->getData();
      //  $piedata=array();
        $idmun = array();
        $idevento = array();
        foreach ($data as $d) {
            // �?าร add element ให้ array ทำได้ 2 รูป�?บบ
          //  array_push($piedata, array($d['hosname'], intval($d['total'])));
            //$piedata[] = array($d['hosname'], intval($d['total']), 6);
            
        array_push($idmun, $d['idmun']);
            //$hosname[] = $d['hosname'];
            
            array_push($idevento, intval($d['idevento']));
            //$total[] = intval($d['total']);
        }

?>
  <?php /*
        $this->widget('ext.booster.widgets.TbHighCharts', array(
            'options' => array(
                'chart' => array(
                    'type' => 'column',
                ),
                'colors' => array('#4EBA0C'),
                'title' => array('text' => 'Eventos'),
                'yAxis' => array(
                    'title' => array('text' => 'municipio')
                ),
                'xAxis' => array(
                    'categories' =>$idmun,
                ),
                'series' => array(
                    array(
                        'name' => 'evento',
                        'data' => $idevento,
                    )
                )
            )
        ));
     */   ?>
