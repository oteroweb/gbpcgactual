<?php
/* @var $this BasicosController */
/* @var $model Basicos */

$this->breadcrumbs = array(
    'Basicoses' => array('index'),
    $model->id,
);

$menu = array();
require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '_menu.php');


$menu2 = array(
    //inicio menu municipios 
    array('label'=>'Crear PDF', 'url'=>array('pdf','id'=>$model->id),
         'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
    array('label'=>'Crear PDF', 'url'=>array('pdf','id'=>$model->id),
         'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')),
    array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
    array('label' => 'Crear', 'url' => array('create2'), 'icon' => 'plus',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')),
    array('label' => 'Actualizar', 'url' => array('update', 'id' => $model->id), 'icon' => 'edit',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
    array('label' => 'Actualizar', 'url' => array('update2', 'id' => $model->id), 'icon' => 'edit',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')),
    array('label' => 'Listado de Eventos', 'url' => array('admin'), 'icon' => 'list-alt',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('administrador')),
     array('label' => 'Listado de Eventos', 'url' => array('admin2'), 'icon' => 'list-alt',
        'visible' => !Yii::app()->user->isGuest && Yii::app()->user->checkAccess('internos_municipios')),
        //fin menu municipios 
        //inicio menu administrador 
        // fin menu administrador
// inicio menu invitados
        // Fin menu invitados
        //inicio menu municipios 	
);

if (!isset($_GET['asModal'])) {
    ?>
    <?php
    $box = $this->beginWidget(
            'bootstrap.widgets.TbBox', array(
        'title' => 'Evento de ' . $model->idevento0->evento,
        'headerIcon' => 'icon-list-alt',
        'headerButtons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButtonGroup',
                'type' => 'success',
                // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons' => $menu2
            ),
        )
            )
    );
    ?>
    <?php
}
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => false, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), //success, info, warning, error or danger
        'info' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), //success, info, warning, error or danger
        'warning' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), //success, info, warning, error or danger
        'error' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), //success, info, warning, error or danger
        'danger' => array('block' => true, 'fade' => true, 'closeText' => '&times;'), //success, info, warning, error or danger
    ),
));
?>


<div class="well well-small">
    <h4>Informacion Basica</h4>
</div>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'type' => 'bordered condensed',
    'htmlOptions' => array(
        'class' => 'table  table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => array(
        //'id',

        array(
            'name' => 'fecha',
            'type' => 'raw',
            'value' => date(' d-m-Y ', strtotime($model->fecha)),
        // 'value' => date("l, d M Y",strtotime($model->fecha)),    
        ),
        'codevento',
        'idmun0.municipio',
        'idcor0.correguimiento',
        'cordx',
        'cordy',
       // 'cordz',
        'dia',
        'idevento0.evento',
        'cal_evento',
        'causa_evento',
        'vulnerabilidad.vulnerabiliad',
        'gradovul',
        'finformacion',
        'descripcion',
        'accionespar',
        'observaciones',
        //'imagen1',
        array(
            'name' => 'imagen1',
            'type' => 'raw',
            'value' => CHtml::image(Yii::app()->baseUrl . '/images/basicos/' . $model->imagen1, 'imagen1', array('width' => 150)),
        ),
       // 'imagen2',
        array(
            'name' => 'imagen2',
            'type' => 'raw',
            'value' => CHtml::image(Yii::app()->baseUrl . '/images/basicos/' . $model->imagen2, 'imagen2', array('width' => 150)),
        ),
      //  'imagen3',
        array(
            'name' => 'imagen3',
            'type' => 'raw',
            'value' => CHtml::image(Yii::app()->baseUrl . '/images/basicos/' . $model->imagen3, 'imagen3', array('width' => 150)),
        ),
        
         array(                        
                  'label'=>'pdf',
                  'type'=>'raw',
                  'value'=>CHtml::link("$model->pdf", Yii::app()->request->hostInfo.'/laguajira/images/basicos/'.$model->pdf,array("target"=>"_blank"))
                ),
       
       // 'pdf',
    /*
      //CONTOH
      array(
      'header' => 'Level',
      'name'=> 'ref_level_id',
      'type'=>'raw',
      'value' => ($model->Level->name),
      // 'value' => ($model->status)?"on":"off",
      // 'value' => @Admin::model()->findByPk($model->createdBy)->username,
      ),

     */
    ),
));
?>
 

<div class="well well-small">
    <h4>Personas Afectadas</h4>
</div>


<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'type' => 'bordered condensed',
    'htmlOptions' => array(
        'class' => 'table  table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => array(
        'Muertos',
        'heridos',
        'damnificados',
        'enfermos',
        'evacuados',
        'desaparecidos',
        'reubicados',
    /*
      //CONTOH
      array(
      'header' => 'Level',
      'name'=> 'ref_level_id',
      'type'=>'raw',
      'value' => ($model->Level->name),
      // 'value' => ($model->status)?"on":"off",
      // 'value' => @Admin::model()->findByPk($model->createdBy)->username,
      ),

     */
    ),
));
?>

<div class="well well-small">
    <h4>Elementos Afectados</h4>
</div>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'type' => 'bordered condensed',
    'htmlOptions' => array(
        'class' => 'table  table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => array(
        'vdestruidas',
        'vafectadas',
        'semovientes',
        'cultivos',
        'viasafectadas',
        'bosque',
        'ceducativo',
        'chospital',
        'valorperdida',
    /*
      //CONTOH
      array(
      'header' => 'Level',
      'name'=> 'ref_level_id',
      'type'=>'raw',
      'value' => ($model->Level->name),
      // 'value' => ($model->status)?"on":"off",
      // 'value' => @Admin::model()->findByPk($model->createdBy)->username,
      ),

     */
    ),
));
?>
 <?php  $this->widget('bootstrap.widgets.TbButton',
         
            array(
                'type' => 'success',
                'label' => 'Crear PDF',
                'url'=>array('pdf','id'=>$model->id),
               
            )
        ); ?>


<?php
if (!isset($_GET['asModal'])) {
    $this->endWidget();
}
?>

