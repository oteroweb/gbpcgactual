<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codevento')); ?>:</b>
	<?php echo CHtml::encode($data->codevento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idmun')); ?>:</b>
	<?php echo CHtml::encode($data->idmun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcor')); ?>:</b>
	<?php echo CHtml::encode($data->idcor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cordx')); ?>:</b>
	<?php echo CHtml::encode($data->cordx); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cordy')); ?>:</b>
	<?php echo CHtml::encode($data->cordy); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cordz')); ?>:</b>
	<?php echo CHtml::encode($data->cordz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dia')); ?>:</b>
	<?php echo CHtml::encode($data->dia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idevento')); ?>:</b>
	<?php echo CHtml::encode($data->idevento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cal_evento')); ?>:</b>
	<?php echo CHtml::encode($data->cal_evento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('causa_evento')); ?>:</b>
	<?php echo CHtml::encode($data->causa_evento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vulnerabilidad_id')); ?>:</b>
	<?php echo CHtml::encode($data->vulnerabilidad_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gradovul')); ?>:</b>
	<?php echo CHtml::encode($data->gradovul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Muertos')); ?>:</b>
	<?php echo CHtml::encode($data->Muertos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heridos')); ?>:</b>
	<?php echo CHtml::encode($data->heridos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('damnificados')); ?>:</b>
	<?php echo CHtml::encode($data->damnificados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enfermos')); ?>:</b>
	<?php echo CHtml::encode($data->enfermos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('evacuados')); ?>:</b>
	<?php echo CHtml::encode($data->evacuados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desaparecidos')); ?>:</b>
	<?php echo CHtml::encode($data->desaparecidos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reubicados')); ?>:</b>
	<?php echo CHtml::encode($data->reubicados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vdestruidas')); ?>:</b>
	<?php echo CHtml::encode($data->vdestruidas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vafectadas')); ?>:</b>
	<?php echo CHtml::encode($data->vafectadas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('semovientes')); ?>:</b>
	<?php echo CHtml::encode($data->semovientes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cultivos')); ?>:</b>
	<?php echo CHtml::encode($data->cultivos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viasafectadas')); ?>:</b>
	<?php echo CHtml::encode($data->viasafectadas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bosque')); ?>:</b>
	<?php echo CHtml::encode($data->bosque); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ceducativo')); ?>:</b>
	<?php echo CHtml::encode($data->ceducativo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chospital')); ?>:</b>
	<?php echo CHtml::encode($data->chospital); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valorperdida')); ?>:</b>
	<?php echo CHtml::encode($data->valorperdida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('finformacion')); ?>:</b>
	<?php echo CHtml::encode($data->finformacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accionespar')); ?>:</b>
	<?php echo CHtml::encode($data->accionespar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen1')); ?>:</b>
	<?php echo CHtml::encode($data->imagen1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen2')); ?>:</b>
	<?php echo CHtml::encode($data->imagen2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagen3')); ?>:</b>
	<?php echo CHtml::encode($data->imagen3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pdf')); ?>:</b>
	<?php echo CHtml::encode($data->pdf); ?>
	<br />

	*/ ?>

</div>