<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'basicos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

	<?php echo $form->errorSummary($model); ?>
   
<?php /* 
Yii::import('ext.gmap.*');
$gMap = new EGMap();
 
$gMap->setWidth(512);
// it can also be called $gMap->height = 400;
$gMap->setHeight(400);
$gMap->zoom = 8; 
 
// set center to inca
$gMap->setCenter(39.719588117933185, 2.9087440013885635);
 
// my house when i was a kid
$home = new EGMapCoord(39.720991014764536, 2.911801719665541);
 
// my ex-school
$school = new EGMapCoord(39.719456079114956, 2.8979293346405166);
 
// some stops on the way
$santo_domingo = new EGMapCoord(39.72118906848983, 2.907628202438368);
$plaza_toros  = new EGMapCoord(39.71945607911511, 2.9049245357513565);
$paso_del_tren = new EGMapCoord( 39.718762871171776, 2.903637075424208 );
 
// Waypoint samples
$waypoints = array(
      new EGMapDirectionWayPoint($santo_domingo),
      new EGMapDirectionWayPoint($plaza_toros, false),
      new EGMapDirectionWayPoint($paso_del_tren, false)
    );
 
// Initialize GMapDirection
$direction = new EGMapDirection($home, $school, 'direction_sample', array('waypoints' => $waypoints));
 
$direction->optimizeWaypoints = true;
$direction->provideRouteAlternatives = true;
 
$renderer = new EGMapDirectionRenderer();
$renderer->draggable = true;
$renderer->panel = "direction_pane";
$renderer->setPolylineOptions(array('strokeColor'=>'#FFAA00'));
 
$direction->setRenderer($renderer);
 
$gMap->addDirection($direction);
 
$gMap->renderMap();
*/?>
<div id="direction_pane"></div>




<div class="row-fluid">
    <?php //echo $form->datepickerRow($model, 'fecha', array('options' => array(), 'htmlOptions' => array('class' => 'span5')), array('prepend' => '<i class="icon-calendar"></i>', 'append' => 'Click on Month/Year at top to select a different year or type in (mm/dd/yyyy).')); ?>
    <div class="span3"><?php
        echo $form->datepickerRow($model, 'fecha', array('options' => array('language' => 'es', 'format' => 'yyyy-mm-dd',),
            'htmlOptions' => array('class' => 'span5')), array('prepend' => '<i class="icon-calendar"></i>',
            'append' => '(año/mes/dia).'));
        ?></div>  
    <div class="span3"><?php echo $form->textFieldRow($model, 'codevento', array('class' => 'span10', 'maxlength' => 50, 'disabled'=>true)); ?></div>  
    <input class="span10" disabled="disabled" placeholder="Codigo" id="Basicosx" type="hidden">
</div>

<div class="row-fluid">
    <div class="span3"><?php echo $form->dropDownListRow($model, 'idmun', 
            Basicos::ObtenerMunicipio(),
            array(
                            'ajax'=>array(
                              'type'=>'POST',
                              'url'=>CController::createUrl('Basicos/comboCorregimientos'),
                              'update'=>'#'.CHtml::activeId($model,'idcor'),
                             'beforeSend' => 'function(){
                               $("#Basicos_idcor").find("option").remove();
                               
                               }',  
                            ),'prompt'=>'Seleccione'
                            
                            
                        )); ?></div>
    <div class="span3"><?php 
                            $lista_dos = array();
                if(isset($model->idcor)){
                $idmun1 = intval($model->idmun); 
                $lista_dos = CHtml::listData(Corregimiento::model()->findAll("idmun = '$idmun1'"),'id','correguimiento');
                } 
                            echo $form->dropDownListRow($model, 'idcor', $lista_dos); ?></div>
</div>

<legend><h5>Coordenadas Geograficas</h5></legend>

<div class="row-fluid">
    
    <div class="span12"><?php 
       $this->widget('ext.RGmapPicker.RGmapPicker',
                array(
                    'title' => 'Location',
                    'element_id' => 'GMapLocation',
                    'map_width' => 500,
                    'map_height' => 250,
                    'map_latitude' => '11.544167', # Your default position
                    'map_longitude' => '-72.90694400000001', # Your default position
                    'map_location_name' => 'Default position name',
                )
            );
        ?></div>
    
    
    <div class="span3"><?php echo $form->textFieldRow($model, 'cordx', array('class' => 'span10', 'maxlength' => 50, 'disabled'=>true)); ?></div>
    <input class="span10" disabled="disabled" placeholder="cordx" id="Basicosx" type="hidden">
    <div class="span3"><?php echo $form->textFieldRow($model, 'cordy', array('class' => 'span10', 'maxlength' => 50, 'disabled'=>true));?></div>
    <input class="span10" disabled="disabled" placeholder="cordy" id="Basicosx" type="hidden">
    <div class="span3"><?php //echo $form->textFieldRow($model, 'cordz', array('class' => 'span10')); ?></div>
</div>
<legend></legend>
<div class="row-fluid">
    <div class="span3"><?php echo $form->textFieldRow($model, 'dia', array('class' => 'span10')); ?></div>
    <div class="span3"><?php echo $form->dropDownListRow($model, 'idevento', Basicos::ObtenerEvento(), array('empty' => '')); ?></div>
    <div class="span3"><?php echo $form->dropDownListRow($model, 'cal_evento', Basicos::ObtenerCalificacionEvento(), array('empty' => '')); ?></div>
</div>

<div class="row-fluid">
    <div class="span4"><?php echo $form->textAreaRow($model, 'causa_evento', array('class' => 'span10', 'rows' => 1)); ?></div>
</div>

<div class="row-fluid">
    <div class="span3"><?php echo $form->dropDownListRow($model, 'vulnerabilidad_id', Efectos::ObtenerVulnerabilidad(), array('empty' => ''), array('class' => 'span10')); ?></div>
    <div class="span3"><?php echo $form->dropDownListRow($model, 'gradovul', Basicos::ObtenerCalificacionEvento(), array('empty' => '')); ?></div>
</div>

<legend><h4>Personas Afectadas</h4></legend>
<p class="note">Digite el numero de Personas afectadas</p>

<div class="row-fluid">
    <div class="span2"><?php echo $form->textFieldRow($model, 'Muertos', array('class' => 'span12')); ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'heridos', array('class' => 'span12')); ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'damnificados', array('class' => 'span12')); ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'enfermos', array('class' => 'span12')); ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'evacuados', array('class' => 'span12')); ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'desaparecidos', array('class' => 'span12')); ?></div>

</div>

<div class="row-fluid">
    <div class="span2"><?php echo $form->textFieldRow($model, 'reubicados', array('class' => 'span12')); ?></div> 

</div>

<legend><h4>Elementos Afectados</h4></legend>

<p class="note">Digite el numero de Elementos afectados</p>

<div class="row-fluid">

    <div class="span3"><?php echo $form->textFieldRow($model, 'vdestruidas', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'vafectadas', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'semovientes', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'cultivos', array('class' => 'span11')); ?></div>
</div>

<div class="row-fluid">

    <div class="span3"><?php echo $form->textFieldRow($model, 'viasafectadas', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'bosque', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'ceducativo', array('class' => 'span11')); ?></div>
    <div class="span3"><?php echo $form->textFieldRow($model, 'chospital', array('class' => 'span11')); ?></div>
</div>


<div class="row-fluid">
    <div class="span3"><?php echo $form->textFieldRow($model, 'valorperdida', array('class' => 'span11')); ?></div>
</div>

<legend></legend>

<div class="row-fluid">
    <div class="span3"><?php echo $form->textFieldRow($model, 'finformacion', array('class' => 'span11', 'maxlength' => 100)); ?></div>
</div>

<div class="row-fluid">
    <div class="span7"> <?php echo $form->textAreaRow($model, 'descripcion', array('class' => 'span12', 'rows' => 4)); ?></div>
</div>

<div class="row-fluid">
    <div class="span7"> <?php echo $form->textAreaRow($model, 'accionespar', array('class' => 'span12', 'rows' => 4)); ?></div>
</div>

<div class="row-fluid">
    <div class="span7"> <?php echo $form->textAreaRow($model, 'observaciones', array('class' => 'span12', 'rows' => 4)); ?></div>
</div>

<legend><h5>Insertar Imagenes</h5></legend>

<div class="row-fluid">
    <div class="span4"><?php echo $form->fileFieldRow($model, 'imagen1');	?>
	<span>
         <?php if(isset($error['imagen1'])) echo $error['imagen1'][0]; ?>
    </span>
</div>

<?php if($model->isNewRecord!='1'){ ?><div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/basicos/'.$model->imagen1,"imagen1",array("width"=>200)); ?>  // La Imagen se muestra aquí si la página es la página de actualización
</div>
 <?php } ?>

    <div class="span4"><?php echo $form->fileFieldRow($model, 'imagen2'); ?>
	<span>
         <?php if(isset($error['imagen2'])) echo $error['imagen2'][0]; ?>
    </span>
	</div> 
	<?php if($model->isNewRecord!='1'){ ?><div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/basicos/'.$model->imagen2,"imagen2",array("width"=>200)); ?>  // La Imagen se muestra aquí si la página es la página de actualización
</div>
	 <?php } ?>
	
    <div class="span4"><?php echo $form->fileFieldRow($model, 'imagen3');?>
<span>
         <?php if(isset($error['imagen3'])) echo $error['imagen3'][0]; ?>
</span>		
	
</div>
<?php if($model->isNewRecord!='1'){ ?><div class="row">
     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/basicos/'.$model->imagen3,"imagen3",array("width"=>200)); ?>  // La Imagen se muestra aquí si la página es la página de actualización
</div>
 <?php } ?>
<legend><h5>Insertar Documentos en Pdf</h5></legend>

<div class="row-fluid">
    <div class="span4"><?php echo $form->fileFieldRow($model, 'pdf'); ?></div>  
	
</div>
<h1> </h1>



<div class="form">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Crear' : 'Guardar',
    ));
    ?>
</div>


<script>


<?php $numero = Basicos::model()->count();
           $numero = str_pad(++$numero, 5, '0', STR_PAD_LEFT);

            ?>


// $( ".gllpLatitude" ).bind( "change", function( ) {
// $("#Basicos_cordx").val($(".gllpLatitude").val());
// });



// $("#xx" ).bind( "change", function( ) {
// console.log("prueba");
// });

$( "#xx" ).change(function() {
  alert( "Handler for .change() called." );
});






$( "#Basicos_fecha" ).bind( "change", function( ) {
var str = $("#Basicos_fecha").val();
var patron="-";
str=str.replace(patron,'');
str=str.replace(patron,'');
var codigo="<?php echo $numero; ?>"
var total=str+codigo;
$("#Basicosx" ).val(total);
});


$( "#Basicos_fecha" ).bind( "change", function( ) {
var str = $("#Basicos_fecha").val();
var patron="-";
str=str.replace(patron,'');
str=str.replace(patron,'');
var codigo="<?php echo $numero; ?>"
var total=str+codigo;
$("#Basicosx" ).val(total);
});




$( "#Basicos_idcor" ).bind( "change", function() {
var total= $("#Basicosx" ).val();
var idcor = $(this).val();
$("#Basicos_codevento" ).val(idcor+total);
});


// $( "input" )
// .change(function() {
// var value = $( this ).val();
// $( "p" ).text( value );
// })
// .change();
</script>



<?php $this->endWidget(); ?>
