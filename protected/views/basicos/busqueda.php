<?php
$this->breadcrumbs=array(
	'Basicoses'=>array('index'),
	'Administrar',
);

$this->menu=array(
array(
        'label' => 'Listado de eventos',
        'itemOptions' => array('class' => 'nav-header')
    ),
array('label' => 'Crear', 'url' => array('create'), 'icon' => 'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('basicos-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<legend><h3>Listado de Eventos de Riesgos</h3></legend>

<p>
    El siguiente cuadro de informacion posee la funcionalidad de realizar consultas por columnas, ademas de mostrar, actualizar, eliminar o exportar la informacion por filas.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'type' => 'bordered',
'id'=>'basicos-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'fecha',
		'codevento',
		'idmun',
		'idcor',
		'cordx',
		/*
		'cordy',
		'cordz',
		'dia',
		'idevento',
		'cal_evento',
		'causa_evento',
		'vulnerabilidad_id',
		'gradovul',
		'Muertos',
		'heridos',
		'damnificados',
		'enfermos',
		'evacuados',
		'desaparecidos',
		'reubicados',
		'vdestruidas',
		'vafectadas',
		'semovientes',
		'cultivos',
		'viasafectadas',
		'bosque',
		'ceducativo',
		'chospital',
		'valorperdida',
		'finformacion',
		'descripcion',
		'accionespar',
		'observaciones',
		'imagen1',
		'imagen2',
		'imagen3',
		'pdf',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
