<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<legend><h5>Informacion Basica requerida</h5></legend>

<div class="row-fluid">
    <div class="span3"><?php
        echo $form->dropDownListRow($model, 'idmun', Basicos::ObtenerMunicipio2(), array(
            'ajax' => array(
                'type' => 'POST',
                'url' => CController::createUrl('Basicos/comboCorregimientos'),
                'update' => '#' . CHtml::activeId($model, 'idcor'),
                'beforeSend' => 'function(){
                               $("#Basicos_idcor").find("option").remove();
                               
                               }',
            ), 'prompt' => 'Seleccione'
        ));
        ?></div>
    <div class="span3"><?php
        $lista_dos = array();
        if (isset($model->idcor)) {
            $idmun1 = intval($model->idmun);
            $lista_dos = CHtml::listData(Corregimiento::model()->findAll("idmun = '$idmun1'"), 'id', 'correguimiento');
        }
        echo $form->dropDownListRow($model, 'idcor', $lista_dos);
        ?>
    </div>    
</div>


<legend><h5>Informacion adicional</h5></legend>

<div class="row-fluid">
    <div class="span3"><?php
        echo $form->datepickerRow($model, 'fecha', array('options' => array('language' => 'es', 'format' => 'yyyy-mm-dd',),
            'htmlOptions' => array('class' => 'span12')), array('prepend' => '<i class="icon-calendar"></i>'));
        ?></div>
    <div class="span2"><?php echo $form->textFieldRow($model, 'dia', array('class' => 'span10')); ?></div>
    <div class="span3"><?php echo $form->dropDownListRow($model, 'idevento', Basicos::ObtenerEvento(),array('empty' => '')); ?></div>
    <div class="span2"><?php echo $form->dropDownListRow($model, 'cal_evento', Basicos::ObtenerCalificacionEventoBusqueda(),array('class' => 'span12')); ?></div>
    
</div>  







<div class="form">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Buscar',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
