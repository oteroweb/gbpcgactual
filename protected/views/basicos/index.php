<?php
/* @var $this BasicosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Basicos',
);

$menu=array();
require(dirname(__FILE__).DIRECTORY_SEPARATOR.'_menu.php');
$this->menu=array(
//	array('label'=>'Basicos','url'=>array('index'),'icon'=>'fa fa-list-alt', 'items' => $menu)	
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('basicos-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");

Yii::app()->clientScript->registerScript('refreshGridView', "
	// automatically refresh grid on 5 seconds
	//setInterval(\"$.fn.yiiGridView.update('basicos-grid')\",5000);
");

?>

<?php $box = $this->beginWidget(
    'bootstrap.widgets.TbBox',
    array(
        'title' => '<h3>Listado de Eventos de Riesgos</h3>' ,
        'headerIcon' => 'icon-list-alt',
        'headerButtons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButtonGroup',
                'type' => 'success',
                // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons' => $this->menu
            ),
        ) 
    )
);?>
<?php /** $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); **/ ?>
		<?php $this->widget('bootstrap.widgets.TbAlert', array(
		    'block'=>false, // display a larger alert block?
		    'fade'=>true, // use transitions?
		    'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
		    'alerts'=>array( // configurations per alert type
		        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		        'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), //success, info, warning, error or danger
		    ),
		));
		?>

<p>
 El siguiente cuadro muestra el listado de Evento ocurrido en el departamento
</p>




<?php echo CHtml::link('Buscar Evento','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php echo CHtml::beginForm(array('export')); ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'basicos-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'type' => 'bordered condensed', //bordered condensed
	'columns'=>array(
		/*array('header'=>'No','value'=>'($this->grid->dataProvider->pagination->currentPage*
					 $this->grid->dataProvider->pagination->pageSize
					)+
					array_search($data,$this->grid->dataProvider->getData())+1',
				'htmlOptions' => array('style' =>'width: 25px; text-align:center;'),
		),*/
		array(
	        'header' => 'Fecha',
	        'name'=> 'fecha',
	        'type'=>'raw',
	        'value' => '(date( " d-m-Y ", strtotime($data->fecha)))',
	        'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
	    ),
		
		array(
	        'name'=> 'codevento',
	        'value' => '($data->codevento)',
	        'headerHtmlOptions' => array('style' => 'text-align:center;'),
	    ),
		
	/*	array(
	        'name'=> 'idmun',
	        'value' => '($data->idmun)',
	        'headerHtmlOptions' => array('style' => 'text-align:center;'),
	    ), */
            
             array('name' => 'idmun',
            'value' => '$data->idmun0->municipio', 'type' => 'text',),
            
             array('name' => 'idcor',
             'value' => '$data->idcor0->correguimiento', 'type' => 'text',),
		
	/*	array(
	        'name'=> 'idcor',
	        'value' => '($data->idcor)',
	        'headerHtmlOptions' => array('style' => 'text-align:center;'),
	    ),*/
		
	/*	array(
	        'name'=> 'cordx',
	        'value' => '($data->cordx)',
	        'headerHtmlOptions' => array('style' => 'text-align:center;'),
	    ),
		
		array(
	        'name'=> 'cordy',
	        'value' => '($data->cordy)',
	        'headerHtmlOptions' => array('style' => 'text-align:center;'),
	    ),*/
		
		//'cordz',
		  'dia',
                  array( 'name' => 'idevento',
                         'value' => '$data->idevento0->evento',
            ),
            
               array(
            'name' => 'cal_evento',
            'value'=>'$data-> cal_evento',
              ),
		'causa_evento',
		//'vulnerabilidad_id',
		//'gradovul',
		//'Muertos',
		//'heridos',
		//'damnificados',
		//'enfermos',
		//'evacuados',
		//'desaparecidos',
		//'reubicados',
		//'vdestruidas',
		//'vafectadas',
		//'semovientes',
		//'cultivos',
		//'viasafectadas',
		//'bosque',
		//'ceducativo',
		//'chospital',
		//'valorperdida',
		//'finformacion',
		//'descripcion',
		//'accionespar',
		//'observaciones',
		//'imagen1',
		//'imagen2',
		//'imagen3',
		//'pdf',

		/*
		//Contoh
		array(
	        'header' => 'Level',
	        'name'=> 'ref_level_id',
	        'type'=>'raw',
	        'value' => '($data->Level->name)',
	        // 'value' => '($data->status)?"on":"off"',
	        // 'value' => '@Admin::model()->findByPk($data->createdBy)->username',
	    ),
	    */
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}',
			'buttons'=>array
            (
                'view' => array
                (    
                	'url' => '$data->id',              
                	'click' => 'function(){
                		data=$(this).attr("href").split("|")
                		$("#myModalHeader").html(data[1]);
	        			$("#myModalBody").load("'.$this->createUrl('view').'&id="+data[0]+"&asModal=true");
                		$("#myModal").modal();
                		return false;
                	}',
                ),
                            )
		),
	),
)); ?>

<!--<select name="fileType" style="width:150px;">
	<option value="Excel5">EXCEL 5 (xls)</option>
	<option value="Excel2007">EXCEL 2007 (xlsx)</option>
	<option value="HTML">HTML</option>
	<option value="PDF">PDF</option>
	<option value="WORD">WORD (docx)</option>
</select>-->
<br>

<?php /*
$this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit', 'icon'=>'fa fa-print','label'=>'Exportar', 'type'=> 'primary'));

 */?>
 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget(); ?>
<?php  $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4 id="myModalHeader">Detalles del Evento</h4>
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>Cargando...</p>
    </div>
 
    <div class="modal-footer">
        <?php  $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Cerrar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php  $this->endWidget(); ?>
